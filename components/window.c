#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <stdio.h>

#include "../util.h"

static Display *dpy;

Bool xerror = False;

int handle_error(){
  xerror = True;
  return 1;
}

void focused_window(Window *w){
	int revert_to;
	XGetInputFocus(dpy, w, &revert_to);
}

const char *window_name(){
	int count, result;
	char **list = NULL;
	Window w;

	focused_window(&w);

	if (xerror){
		xerror = False;
		return bprintf("%-64s", "");
	}

	Atom net_wm_name = XInternAtom(dpy, "_NET_WM_NAME", False);
	XTextProperty prop;
	Status s;

	s = XGetTextProperty(dpy, w, &prop, net_wm_name) || XGetWMName(dpy, w, &prop);
	if(s){
		result = XmbTextPropertyToTextList(dpy, &prop, &list, &count); // see man
		if(result >= Success && count > 0 && *list){
			return bprintf("%-64s", list[0]);
		}
	}
	return bprintf("%-64s", "");
}

void get_windows(Window windows[], int *nums)
{
	Window rootWindow = XDefaultRootWindow(dpy);
	Atom client_list = XInternAtom(dpy, "_NET_CLIENT_LIST" , True);
	Atom actualType;
	int format;
	unsigned long numItems, bytesAfter;
	unsigned char *data = 0;
	int status = XGetWindowProperty(dpy,
								rootWindow,
								client_list,
								0L,
								(~0L),
								False,
								AnyPropertyType,
								&actualType,
								&format,
								&numItems,
								&bytesAfter,
								&data);

	if (status >= Success && numItems && format == 32)
	{
		*nums = numItems;
		long int *array = (long int*) data;
		for (int k = 0; k < (int) numItems; k++)
		{
			Window ww = (Window) array[k];
			windows[k] = ww;
		}
		XFree(data);
	}
}

void get_current(int *current)
{
	Window rootWindow = XDefaultRootWindow(dpy);
	Atom current_desktop = XInternAtom(dpy, "_NET_CURRENT_DESKTOP" , True);
	Atom actualType;
	int format;
	unsigned long numItems, bytesAfter;
	unsigned char *data = 0;
	int status = XGetWindowProperty(dpy,
								rootWindow,
								current_desktop,
								0L,
								(~0L),
								False,
								AnyPropertyType,
								&actualType,
								&format,
								&numItems,
								&bytesAfter,
								&data);

	if (status >= Success && numItems && format == 32)
	{
		*current = data[0];
		XFree(data);
	}
}

void get_desktop(Window w, int*desktop){
	Atom wm_state = XInternAtom(dpy, "_NET_WM_DESKTOP", False);
	Atom actualType;
	int format;
	unsigned long numItems, bytesAfter;
	unsigned char *data = 0;
	int status = XGetWindowProperty(dpy,
								w,
								wm_state,
								0L,
								(~0L),
								False,
								AnyPropertyType,
								&actualType,
								&format,
								&numItems,
								&bytesAfter,
								&data);

	if (status >= Success && numItems)
	{
		*desktop = data[0];
		XFree(data);
	}
}

void get_state(Window w, int*state){
	Atom wm_state = XInternAtom(dpy, "WM_STATE", False);
	Atom actualType;
	int format;
	unsigned long numItems, bytesAfter;
	unsigned char *data = 0;
	int status = XGetWindowProperty(dpy,
								w,
								wm_state,
								0L,
								(~0L),
								False,
								AnyPropertyType,
								&actualType,
								&format,
								&numItems,
								&bytesAfter,
								&data);

	if (status >= Success && numItems && format == 32)
	{
		*state = data[0];
		XFree(data);
	}
}

const char *window_groups(){
	Window windows[128];
	int numItems = 0, current = 0;
	int desktops[9] = {0,0,0,0,0,0,0,0,0};
	get_windows(windows, &numItems);
	get_current(&current);
	char out[256];
	int focused=0;
	Window fw;
	focused_window(&fw);
	get_desktop(fw, &focused);

	for (int k = 0; k < numItems; k++)
	{
		Window ww = windows[k];
		int desktop = 0;
		int state = 0;
		get_desktop(ww, &desktop);
		get_state(ww, &state);
		if (desktop<10 && desktop > 0 && !desktops[desktop - 1]){
			desktops[desktop - 1] = 1;
			if (state == 1)
				desktops[desktop - 1] = 2;
		}
	}
	int index = 0;
	for (int i = 0; i < 9; i++){
		if (i+1 == current){
			index += sprintf(&out[index], "%s%d%s ", "\033[0;01m", i+1, "\033[0;00m");
		}
		else if (desktops[i] == 1){
			index += sprintf(&out[index], "%s%d%s ", "\033[30;1m", i+1, "\033[0;00m");
		}
		else if (desktops[i] == 2){
			index += sprintf(&out[index], "%s%d%s ", "\033[0;00m", i+1, "\033[0;00m");
		}
		else {
			index += sprintf(&out[index], "%s", "\033[0;00m\033[0;00m");
		}
	}
	index += sprintf(&out[index], "\b](%d)", focused);
	return bprintf("[%s", out);
}


int getDisplay(){
	if (!(dpy = XOpenDisplay(NULL))) {
		return 0;
	}
	XSetErrorHandler(handle_error);
	return 1;
}
