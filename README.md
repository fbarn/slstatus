## slstatus
slstatus is a suckless status monitor for window managers that use WM_NAME
(e.g. dwm) or stdin to fill the status bar.

![screenshot](image.png)

## Description
This fork includes support for window information (e.g. active desktops, selected window) useful for running outside of dwm.

## Features
- Battery percentage/state/time left
- CPU usage
- CPU frequency
- Custom shell commands
- Date and time
- Disk status (free storage, percentage, total storage and used storage)
- Available entropy
- Username/GID/UID
- Hostname
- IP address (IPv4 and IPv6)
- Kernel version
- Keyboard indicators
- Keymap
- Load average
- Network speeds (RX and TX)
- Number of files in a directory (hint: Maildir)
- Memory status (free memory, percentage, total memory and used memory)
- Swap status (free swap, percentage, total swap and used swap)
- Temperature
- Uptime
- Volume percentage
- WiFi signal percentage and ESSID
- Active window title and desktop
- Active desktops (distinction between current, visible, and hidden)

## Installation
Edit config.mk to match your local setup (slstatus is installed into the
/usr/local namespace by default).

Afterwards enter the following command to build and install slstatus (if
necessary as root):

    make clean install

## Running slstatus
See the man page for details.

Tested on Linux (dwm) and OpenBSD (cwm).

Will be picked up by dwm statusbar by running the following at X startup:

    slstatus
    
For all other WM's, can be run in the following way to print status to stdout:

    slstatus -s
    
Example of how it can be run in cwm (by putting the following in some wrapper script):

	while true;
	do
	    xterm -class slstatus -T slstatus -geometry 142x1+0+0 -e '.local/bin/slstatus -s';
    done

## Configuration
slstatus can be customized by creating a custom config.h and (re)compiling the
source code. This keeps it fast, secure and simple.

## Related and Referenced Projects
[slstatus](https://tools.suckless.org/slstatus/)

[termbar](https://github.com/gonzalo-/termbar)

[A simple shell status bar for OpenBSD and cwm](https://www.tumfatig.net/2020/a-simple-shell-status-bar-for-openbsd-and-cwm1/)
